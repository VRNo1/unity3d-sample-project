#!/bin/sh


function findLogFileArgument() {
  args=( "$@" )
  len=${#args[@]}
  LOGARG=
  for (( i=0;i<$len;i++)); do 
    #echo "${args[${i}]}"
    if [ "${args[${i}]}" == "-logFile" ]; then
      pos=$(($i + 1))
      if [[ $pos -lt $len ]]; then
        LOGARG="${args[${pos}]}"
      fi
    fi
  done
  echo $LOGARG
}

# Decide on platform (defaults to UNIX)
PLATFORM="UNIX"
if [ "$LOCALAPPDATA" != "" ]; then
  PLATFORM="WIN"
fi

# Platform specific stuff
if [ $PLATFORM == "WIN" ]; then
  TAIL_PREFIX="tail --follow=name "
  DEFAULT_UNITY_EDITOR_LOG="$LOCALAPPDATA/Unity/Editor/editor.log"
  UNITY_APPENDIX="/Editor/Unity.exe"
fi

if [ $PLATFORM ==  "UNIX" ]; then
  TAIL_PREFIX="tail -F "
  DEFAULT_UNITY_EDITOR_LOG="$HOME/Library/Logs/Unity/Editor.log"
  UNITY_APPENDIX="/Contents/MacOS/Unity"
fi

# Deciding on LOG file
LOG=$(findLogFileArgument "$@")

# VAR Clash - this shouldn't happen
if [ -n "$UNITY_EDITOR_LOG" ]; then
  if [ -n "$LOG" ]; then
    echo "WARNING: both -logFile and UNITY_EDITOR_LOG passed"
  fi
fi

if [ -n "$LOG" ]; then
  UNITY_EDITOR_LOG="$LOG"
  echo "Using editor.log $UNITY_EDITOR_LOG (from command line arguments)"
fi

# default to platform specific log
if [ "$UNITY_EDITOR_LOG" == "" ]; then
  UNITY_EDITOR_LOG="$DEFAULT_UNITY_EDITOR_LOG"
  echo "Using default editor.log $UNITY_EDITOR_LOG"
fi

if [ -z "$UNITY" ]; then
  if [ -n "$UNITY_HOME" ]; then
    UNITY="$UNITY_HOME$UNITY_APPENDIX"
    echo "Using Unity $UNITY (derived from UNITY_HOME environment variable)"
  else
    echo "Neither UNITY nor UNITY_HOME defined. Cannot execute"
    exit -1
  fi
else
  echo "Using Unity $UNITY (from UNITY environment variable)"
fi

#clear existing log and start tail to echo log to stdout
rm -f "$UNITY_EDITOR_LOG"
touch "$UNITY_EDITOR_LOG"
$TAIL_PREFIX $UNITY_EDITOR_LOG &

#start unity
echo "$UNITY" "$@"
"$UNITY" "$@"
EXITCODE="$?"
echo "Unity exited with exitcode $EXITCODE"

#stop echoing log
kill %1

#exit with the same exit code as unity
exit "$EXITCODE"
